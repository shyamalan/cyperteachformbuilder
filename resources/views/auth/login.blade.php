@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form  aria-label="{{ __('Login') }}" id="myForm">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <!-- <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> -->

                                    <!-- <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label> -->
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="button" class="btn btn-primary" id="loginBtn">
                                 <a class="loaded" > {{ __('Login') }}</a>
                                 <a class="loading" style="display:none">Loading...</a>
                                </button> 
                                
                                <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a> -->
                                <!-- <button class="btn btn-primary" type="button" disabled>
  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
  Loading...
</button> -->                
                            </div>
                        </div>
                    </form>
                    <div class="alert alert-danger m-4 pull-right fieldcomplete" role="alert" style="display:none"><div class="errorfield"></div></div>
                    <div class="alert alert-danger m-4 ValideLogin" role="alert" style="display:none">In Valide Login</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
            $(function() {
                    $('#loginBtn').on('click', function() {
                    var formData = $('#myForm').serializeArray();
                    $('.loading').show();
                    $('.loaded').hide();
                    $('.ValideLogin').hide();
                    $('.fieldcomplete').hide();
                    $.ajax({
                        type: "POST",
                        url: "Customlogin",
                        data: formData,
                        success: function (response) {
                        if(response.error) {
                            $('.loading').hide();
                            $('.loaded').show();
                            $('.ValideLogin').hide();
                            $('.fieldcomplete').show();
                             $.each(response.error,function(key, value){
                              $('.errorfield').text(value);
                             console.log(value);
                            });
   
                        }
                       if(response.successrecord){
                        location.href = 'formbuilder';
                       }  
                        if(response.errorrecord) {
                         $('.loading').hide();
                         $('.loaded').show();   
                         $('.ValideLogin').show();
                         $('.fieldcomplete').hide();
                       }               
                        },
                    });
                   
                });    
            });
        </script>
@endsection
