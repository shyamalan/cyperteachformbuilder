<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use auth;
use App\User;
use Validator;


class Customlogincontroller extends Controller
{
    
    public function Customlogin(Request $request){
       $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if ($validator->passes()) {
                if(Auth::attempt(['email' => Request('email'),'password'  => Request('password')])){
                    return response()->json(['successrecord'=>'success']);
              }
                else{
                    return response()->json(['errorrecord'=>'In Valid UserName']);
                }  
        }
    	return response()->json(['error'=>$validator->errors()->all()]);
    }
}
