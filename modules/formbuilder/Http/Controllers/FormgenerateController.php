<?php

namespace Modules\formbuilder\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DB;
use Modules\Models\Posts;

class FormgenerateController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
      //return Posts::all();


      $tables = DB::select('SHOW TABLES');
      return view('formbuilder::formgenerate',compact('tables'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('formbuilder::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
      return dd($request->all());

      $myfile = fopen("newfile.php", "w") or die("Unable to open file!");
      $txt = "Mickey Mouse\n";
      fwrite($myfile, $txt);
      $txt = "Minnie Mouse\n";
      fwrite($myfile, $txt);
      fclose($myfile);
      //return dd($request->all());
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('formbuilder::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('formbuilder::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
