const mix = require('laravel-mix');

const appPublicPath = global.process.env.MIX_PUBLIC_PATH || 'public';

const appModule = global.process.env.MIX_PACKAGE;

mix.setPublicPath(`${appPublicPath}/modules/formbuilder`);

if (mix.inproduction()) {
    mix.version();
}

mix
    .js(__dirname + '/resources/assets/js/app.js', 'js/formbuilder.js')
    .sass(__dirname + '/resources/assets/sass/app.scss', 'css/formbuilder.css');