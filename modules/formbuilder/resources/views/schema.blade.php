@extends('formbuilder::layouts.master')
  @section('content')
 <div class="container">
   <br>
       <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Shema Builder Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="{{ route('formbuilder.store') }}">
                   @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" id="inputEmail3" placeholder="Name">
                  </div>
                </div>
                <div class="form-group">
                  <label for="Category" class="col-sm-2 control-label">Category :</label>
                  <div class="col-sm-10">
                    <!-- <input type="password" class="form-control" id="inputPassword3" placeholder="Password"> -->

                    <label><input type="checkbox" name="category[]" value="Controller"> Controller</label>
                    <label><input type="checkbox" name="category[]" value="Migration">Migration</label>
                    <label><input type="checkbox" name="category[]" value="View"> View</label>
                    <label><input type="checkbox" name="category[]" value="Model"> Model</label>

                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Sign in</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
      @stop
