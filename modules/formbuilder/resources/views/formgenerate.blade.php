@extends('formbuilder::layouts.master')
  @section('content')
        <div class="container ">
            <nav class="navbar navbar-light  bg-faded fixed-top">
                <div class="clearfix">
                    <div class="container">
                        <button style="cursor: pointer;display: none" savehtml() class="btn btn-success export_html savehtml mt-2 pull-right">Export HTML</button>
                        <h3 class="mr-auto">Form Builder</h3>
                    </div>
                </div>
            </nav>
            @if (\Session::has('success'))
    <div class="alert alert-success alert-dismissible">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
    @endif
            <br/>
            <div class="clearfix"></div>
            <div class="form_builder">
                <div class="row">
                    <div class="col-sm-3">
                        <nav class="nav-sidebar">
                            <ul class="nav">
                                <li class="form_bal_textfield border-left-color-warning border-primary ">
                                    <a class="" href="javascript:;">Text Field <i class="fa fa-plus-circle text-warning  pull-right"></i></a>
                                </li>
                                <li class="form_bal_textarea">
                                    <a href="javascript:;">Text Area <i class="fa fa-plus-circle text-warning pull-right"></i></a>
                                </li>
                                <li class="form_bal_select">
                                    <a href="javascript:;">Select <i class="fa fa-plus-circle text-warning pull-right"></i></a>
                                </li>
                                <li class="form_bal_radio">
                                    <a href="javascript:;">Radio Button <i class="fa fa-plus-circle text-warning pull-right"></i></a>
                                </li>
                                <li class="form_bal_checkbox">
                                    <a href="javascript:;">Checkbox <i class="fa fa-plus-circle text-warning pull-right"></i></a>
                                </li>
                                <li class="form_bal_email">
                                    <a href="javascript:;">Email <i class="fa fa-plus-circle text-warning pull-right"></i></a>
                                </li>
                                <li class="form_bal_number">
                                    <a href="javascript:;">Number <i class="fa fa-plus-circle text-warning pull-right"></i></a>
                                </li>
                                <li class="form_bal_password">
                                    <a href="javascript:;">Password <i class="fa fa-plus-circle text-warning pull-right"></i></a>
                                </li>
                                <li class="form_bal_date">
                                    <a href="javascript:;">Date <i class="fa fa-plus-circle text-warning pull-right"></i></a>
                                </li>
                                <li class="form_bal_button">
                                    <a href="javascript:;">Button <i class="fa fa-plus-circle text-warning pull-right"></i></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-5 bal_builder">
                        <div class="form_builder_area"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <form class="form-horizontal">
                                <div class="preview"></div>
                                <div style="display: none" class="form-group plain_html"><textarea rows="50" class="form-control"></textarea></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>

        <div class="modal-body">
          <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-2">Tablename:</div>
              <div class="col-sm-8">
                <select class="form-control" id="sel1" onchange="getComboA(this)">
                <option value="">--section--</option>
                <?php
                $arr1 = $tables;
                unset($arr1[0],$arr1[1]);
                foreach ($arr1 as $key => $value){
                      foreach ($value as $key => $valuename) {
                      ?>
                      <option value="<?php echo $valuename ?>"><?php echo $valuename ?></option>
                      <?php
                    }
                  }

                 ?>
                 </select>
              </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-2"></div>
              <div class="col-sm-8">
                <div class="juu" style="display:none">
                  </div>
              </div>
            <div class="col-sm-1"></div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

      @stop
<script>
function getComboA(selectObject) {
var value = selectObject.value;
$('.juu').show();
 $('.juu').html('<form class="row" method="POST" action="'+value+'"> @csrf'+$('.juu').html()+'</form>');
}
</script>
