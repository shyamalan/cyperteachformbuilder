<?php

Route::group(['middleware' => 'web', 'prefix' => 'formbuilder', 'namespace' => 'Modules\formbuilder\Http\Controllers'], function()
{
    Route::get('/', 'CartController@index')->name('formbuilder.index');
    Route::get('/schema','ShemasController@index')->name('formbuilder.schema');
    Route::get('/formgenerate','FormgenerateController@index')->name('formbuilder.formgenerate');
    Route::post('/formgeneratecreate','FormgenerateController@store')->name('formbuilder.store');
    Route::resource('posts','PostsController');
    Route::resource('students','StudentsController');

});
